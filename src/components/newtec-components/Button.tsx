import React from 'react';
import classNames from 'classnames';

type BtnColor =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark'
  | 'link';

type BtnSize = 'lg' | 'sm';

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  outline?: boolean;
  active?: boolean;
  block?: boolean;
  color?: BtnColor;
  disabled?: boolean;
  size?: BtnSize;
  className?: string;
}

const Button: React.FC<ButtonProps> = ({
  outline,
  active,
  block,
  color = 'secondary',
  disabled,
  size,
  className,
  ...attributes
}) => {
  const btnColor = `btn${outline ? '-outline' : ''}-${color}`;
  const classes = classNames(
    className,
    'btn',
    btnColor,
    size && `btn-${size}`,
    block && `btn-block`,
    { disabled }
  );
  return <button className={classes} {...attributes} />;
};

export default Button;
