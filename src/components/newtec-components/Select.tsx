import React from 'react';
import classNames from 'classnames';

interface SelectOption {
  value: string;
  label: string;
}

export interface SelectProps
  extends React.InputHTMLAttributes<HTMLSelectElement> {
  name: string;
  label: string;
  options: SelectOption[];
  multiple?: boolean;
  disabled?: boolean;
  valid?: boolean;
  validMsg?: string;
  invalid?: boolean;
  invalidMsg?: string;
  helpTxt?: string;
  custom?: boolean;
  className?: string;
}

const Select: React.FC<SelectProps> = ({
  name,
  label,
  options,
  multiple,
  disabled,
  valid,
  validMsg,
  invalid,
  invalidMsg,
  helpTxt,
  custom = true,
  className,
  ...attributes
}) => {
  const selectClass = custom ? 'custom-select' : 'form-control';
  const classes = classNames(className, selectClass, {
    'is-valid': valid,
    'is-invalid': invalid,
  });

  return (
    <div className="form-group">
      <label className="form-control-label" htmlFor={name}>
        {label}
      </label>
      <select
        name={name}
        className={classes}
        {...attributes}
        multiple={multiple}
        disabled={disabled}
      >
        {options.map(({ label, value }) => (
          <option value={value} key={value}>
            {label}
          </option>
        ))}
      </select>

      {valid && validMsg && <div className="valid-feedback">{validMsg}</div>}
      {invalid && invalidMsg && (
        <div className="invalid-feedback">{invalidMsg}</div>
      )}
      {helpTxt && <small className="form-text text-muted">{helpTxt}</small>}
    </div>
  );
};

export default Select;
