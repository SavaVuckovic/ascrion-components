import React from 'react';

// TODO: add invalid and other properties like Input and Select
interface FileUploadProps {
  name: string;
}

const FileUpload: React.FC<FileUploadProps> = ({ name }) => {
  return (
    <div className="form-group">
      <label htmlFor="uploadInputSpan">Upload file</label>
      <div className="input-group mb-3">
        <div className="custom-file">
          <input type="file" className="custom-file-input" id={name} />
          <label className="custom-file-label" htmlFor={name}>
            Choose file
          </label>
        </div>
        <div className="input-group-append">
          <span className="input-group-text" id="uploadInputSpan">
            Upload
          </span>
        </div>
      </div>
    </div>
  );
};

export default FileUpload;
