import React from 'react';
import classNames from 'classnames';

export interface CheckboxProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label: string;
  valid?: boolean;
  validMsg?: string;
  invalid?: boolean;
  invalidMsg?: string;
  className?: string;
}

const Checkbox: React.FC<CheckboxProps> = ({
  value,
  name,
  label,
  valid,
  validMsg,
  invalid,
  invalidMsg,
  ...attributes
}) => {
  const classes = classNames('custom-control-input', {
    'is-valid': valid,
    'is-invalid': invalid,
  });

  return (
    <div className="custom-control custom-checkbox">
      <input
        id={name}
        name={name}
        type="checkbox"
        className={classes}
        {...attributes}
      />
      <label htmlFor={name} className="custom-control-label">
        {label}
      </label>
      {valid && validMsg && <div className="valid-feedback">{validMsg}</div>}
      {invalid && invalidMsg && (
        <div className="invalid-feedback">{invalidMsg}</div>
      )}
    </div>
  );
};

export default Checkbox;
