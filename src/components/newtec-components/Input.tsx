import React from 'react';
import classNames from 'classnames';

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label: string;
  type?: string;
  valid?: boolean;
  validMsg?: string;
  invalid?: boolean;
  invalidMsg?: string;
  helpTxt?: string;
  className?: string;
}

const Input: React.FC<InputProps> = ({
  name,
  label,
  type = 'text',
  valid,
  validMsg,
  invalid,
  invalidMsg,
  helpTxt,
  className,
  ...attributes
}) => {
  const classes = classNames(className, 'form-control', {
    'is-valid': valid,
    'is-invalid': invalid,
  });

  return (
    <div className="form-group">
      <label className="form-control-label" htmlFor={name}>
        {label}
      </label>
      <input
        id={name}
        name={name}
        type={type}
        className={classes}
        {...attributes}
      />

      {/* TODO: this part is identical in Select, refactor.. */}
      {valid && validMsg && <div className="valid-feedback">{validMsg}</div>}
      {invalid && invalidMsg && (
        <div className="invalid-feedback">{invalidMsg}</div>
      )}
      {helpTxt && <small className="form-text text-muted">{helpTxt}</small>}
    </div>
  );
};

export default Input;
