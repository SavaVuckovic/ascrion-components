import * as yup from 'yup';

const pwRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
const pwMsg = `Password must contain at least one number and one capital letter and minimum 8 characters`;

export const registerSchema = yup.object().shape({
  firstName: yup.string().required('First name cannot be empty'),
  lastName: yup.string().required('Last name cannot be empty'),
  email: yup.string().email().required('Email field cannot be empty'),
  password: yup
    .string()
    .required('Password field cannot be empty')
    .matches(pwRegex, pwMsg),
  repeatPassword: yup
    .string()
    .required('Please confirm your password')
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
  country: yup.string().required('You have to select a country'),
  policy: yup
    .boolean()
    .oneOf([true], 'You must agree before submitting')
    .required('You must agree before submitting'),
  terms: yup
    .boolean()
    .oneOf([true], 'You must agree before submitting')
    .required('You must agree before submitting'),
});

export type RegisterFormValues = yup.InferType<typeof registerSchema>;
