import React, { Component } from 'react';
import { Formik, Form, FormikValues, FormikHelpers } from 'formik';

import Field from '../../common/Field';
import SelectField from '../../common/SelectField';
import CheckboxField from '../../common/CheckboxField';
import Button from '../../newtec-components/Button';
import { registerSchema, RegisterFormValues } from './validation';

const countrySelectItems = [
  { label: 'Serbia', value: 'SR' },
  { label: 'United States', value: 'US' },
  { label: 'United Kingdom', value: 'UK' },
];

const initialValues: RegisterFormValues = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  repeatPassword: '',
  country: '',
  policy: false,
  terms: false,
};

export default class RegisterForm extends Component {
  handleSubmit = (
    values: FormikValues,
    { setSubmitting, resetForm }: FormikHelpers<RegisterFormValues>
  ): void => {
    console.log(values);

    setSubmitting(true);
    setTimeout(() => {
      setSubmitting(false);
      resetForm();
    }, 2000);
  };

  render() {
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={registerSchema}
        onSubmit={this.handleSubmit}
      >
        {({ isSubmitting, isValid, dirty }) => (
          <Form>
            <Field
              name="firstName"
              label="First Name"
              placeholder="First Name"
            />
            <Field name="lastName" label="Last Name" placeholder="Last Name" />
            <Field
              name="email"
              type="email"
              label="Email Address"
              placeholder="Email Address"
              helpTxt="We'll never share your email with anyone else."
            />
            <Field
              name="password"
              type="password"
              label="Password"
              placeholder="Password"
            />
            <Field
              name="repeatPassword"
              type="password"
              label="Repeat Password"
              placeholder="Repeat Password"
            />
            <SelectField
              name="country"
              label="Country"
              placeholder="Select your country"
              options={countrySelectItems}
            />

            <div className="form-group">
              <CheckboxField
                name="policy"
                label="I agree with privacy policy *"
              />
              <CheckboxField name="terms" label="I agree with terms of use *" />
            </div>

            <p>
              <small>
                We will use your personal data in order to manage your request
                and to send you information by electronic means about the
                activities and services offered on this platform. For more
                details on the use of your personal data please read our Privacy
                Policy.
              </small>
            </p>

            {/* TODO: convert this to 'next' button that leads to registration step 2 later */}
            <Button
              block
              size="lg"
              type="submit"
              color="primary"
              className="mt-4"
              disabled={isSubmitting || !isValid || !dirty}
            >
              Register
            </Button>
          </Form>
        )}
      </Formik>
    );
  }
}
