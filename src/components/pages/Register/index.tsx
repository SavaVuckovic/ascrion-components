import React from 'react';
import { Link } from 'react-router-dom';

import AuthPageLayout from '../../common/AuthPageLayout';
import RegisterForm from './RegisterForm';

const Register: React.FC = () => (
  <AuthPageLayout imageUrl="https://fakeimg.pl/800x970/3A3D3F/646464/?retina=1">
    <div className="suggested-link">
      Existing User? Click here to {` `}
      <Link to="/login">Sign In</Link>
    </div>

    <div className="auth-form-wrapper col-lg-6">
      <h1>Registration</h1>
      <RegisterForm />
    </div>
  </AuthPageLayout>
);

export default Register;
