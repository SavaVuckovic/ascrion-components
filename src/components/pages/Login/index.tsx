import React from 'react';
import { Link } from 'react-router-dom';

import AuthPageLayout from '../../common/AuthPageLayout';
import LoginForm from './LoginForm';

const Login: React.FC = () => (
  <AuthPageLayout imageUrl="https://fakeimg.pl/800x970/3A3D3F/646464/?retina=1">
    <div className="suggested-link">
      You don't have an account? Click here to {` `}
      <Link to="/register">Register</Link>
    </div>

    <div className="auth-form-wrapper col-lg-6">
      <h1>Sign In</h1>
      <LoginForm />
      <div className="form-link">
        You don't have an account? Click here to {` `}
        <Link to="/register">Register</Link>
      </div>
    </div>
  </AuthPageLayout>
);

export default Login;
