import * as yup from 'yup';

export const loginSchema = yup.object().shape({
  email: yup.string().email().required('Email field cannot be empty'),
  password: yup.string().required('Password field cannot be empty'),
});

export type LoginFormValues = yup.InferType<typeof loginSchema>;
