import React, { Component } from 'react';
import { Formik, Form, FormikValues, FormikHelpers } from 'formik';

import Field from '../../common/Field';
import Button from '../../newtec-components/Button';
import { loginSchema, LoginFormValues } from './validation';

const initialValues: LoginFormValues = {
  email: '',
  password: '',
};

export default class LoginForm extends Component {
  handleSubmit = (
    values: FormikValues,
    { setSubmitting, resetForm }: FormikHelpers<LoginFormValues>
  ): void => {
    console.log(values);

    setSubmitting(true);
    setTimeout(() => {
      setSubmitting(false);
      resetForm();
    }, 2000);
  };

  render() {
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={loginSchema}
        onSubmit={this.handleSubmit}
      >
        {({ isSubmitting, isValid, dirty }) => (
          <Form>
            <Field
              name="email"
              type="email"
              label="Email Address"
              placeholder="Enter email"
            />

            <Field
              name="password"
              type="password"
              label="Password"
              placeholder="Password"
            />

            <Button
              block
              size="lg"
              type="submit"
              color="primary"
              className="mt-4"
              disabled={isSubmitting || !isValid || !dirty}
            >
              Sign In
            </Button>
          </Form>
        )}
      </Formik>
    );
  }
}
