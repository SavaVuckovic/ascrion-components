import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import MyButton from './newtec-components/Button';
import FileUpload from './newtec-components/FileUpload';

const countrySelectItems = [
  { name: 'Serbia', code: 'SR' },
  { name: 'United States', code: 'US' },
  { name: 'United Kingdom', code: 'UK' },
];

export default class Testing extends Component {
  render() {
    return (
      <>
        <div style={{ border: '1px solid black' }}>
          <h1 style={{ textAlign: 'center' }}>Buttons</h1>
          <Button color="primary" className="has-error">
            Example
          </Button>
          <Button disabled>Example</Button>
          <Button color="success">Example</Button>
          <Button color="warning">Warning</Button>
          <Button color="danger">Danger</Button>
          <Button color="danger" outline>
            Danger
          </Button>
          <Button color="warning" size="lg" outline>
            Danger
          </Button>
          <Button color="warning" size="sm" outline>
            Danger
          </Button>
          <Button color="warning">Close Me</Button>
          <Button size="lg" color="warning">
            Close Me
          </Button>
          <Button block>BUTTON BLOCK</Button>
          <Button disabled>BUTTON DISABLED</Button>
          <Button onClick={(e) => console.log('clicked: ', e)}>Click me</Button>
          <Button color="info">info</Button>
          <Button color="light">light</Button>
          <Button color="dark">dark</Button>
          <Button color="link">link</Button>
        </div>
        <div style={{ border: '1px solid black' }}>
          <h1 style={{ textAlign: 'center' }}>Now my buttons</h1>
          <MyButton color="primary" className="has-error">
            Example
          </MyButton>
          <MyButton disabled>Example</MyButton>
          <MyButton color="success">Example</MyButton>
          <MyButton color="warning">Warning</MyButton>
          <MyButton color="danger">Danger</MyButton>
          <MyButton color="danger" outline>
            Danger
          </MyButton>
          <MyButton color="warning" size="lg" outline>
            Danger
          </MyButton>
          <MyButton color="warning" size="sm" outline>
            Danger
          </MyButton>
          <MyButton color="warning">Close Me</MyButton>
          <MyButton size="lg" color="warning">
            Close Me
          </MyButton>
          <MyButton block>BUTTON BLOCK</MyButton>
          <MyButton disabled>BUTTON DISABLED</MyButton>
          <MyButton outline onClick={(e) => console.log('clicked: ', e)}>
            Click me
          </MyButton>
          <MyButton color="info">info</MyButton>
          <MyButton color="light">light</MyButton>
          <MyButton color="dark">dark</MyButton>
          <MyButton color="link">link</MyButton>
        </div>
        <div style={{ border: '1px solid black', marginTop: '10px' }}>
          <h1 style={{ textAlign: 'center' }}>Form</h1>
          <Form className="form">
            <FormGroup>
              <Label for="firstName">First Name</Label>
              <Input
                name="firstName"
                type="text"
                placeholder="First Name"
                valid
              />
            </FormGroup>
            <FormGroup>
              <Label for="lastName">Last Name</Label>
              <Input
                name="lastName"
                type="text"
                placeholder="Last Name"
                invalid
              />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email Address</Label>
              <Input name="email" type="email" placeholder="Email Address" />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input name="password" type="password" placeholder="Password" />
            </FormGroup>
            <FormGroup>
              <Label for="country">Country</Label>
              <Input type="select" name="country">
                {countrySelectItems.map(({ name, code }) => (
                  <option key={code} value={code}>
                    {name}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="bio">Bio</Label>
              <Input type="textarea" name="bio" />
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" /> Accept the terms or whatever
              </Label>
            </FormGroup>
          </Form>
        </div>
        <div style={{ border: '1px solid red' }}>
          <h1>My form</h1>
          <FileUpload name="test" />
        </div>
        <div style={{ border: '1px solid red' }}>
          <h1>Random</h1>
          <Col></Col>
        </div>
      </>
    );
  }
}
