import React from 'react';
import { useField } from 'formik';

import Select, { SelectProps } from '../newtec-components/Select';

const SelectField: React.FC<SelectProps> = ({ name, ...properties }) => {
  const [field, meta] = useField(name);
  const hasError = meta.error && meta.touched;

  return (
    <Select
      {...field}
      invalid={!!hasError}
      invalidMsg={meta.error}
      {...properties}
    />
  );
};

export default SelectField;
