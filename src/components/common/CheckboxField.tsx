import React from 'react';
import { useField } from 'formik';

import Checkbox, { CheckboxProps } from '../newtec-components/Checkbox';

const CheckboxField: React.FC<CheckboxProps> = ({ name, ...properties }) => {
  const [field, meta] = useField(name);
  const hasError = meta.error && meta.touched;

  return (
    <Checkbox
      {...field}
      invalid={!!hasError}
      invalidMsg={meta.error}
      {...properties}
    />
  );
};

export default CheckboxField;
