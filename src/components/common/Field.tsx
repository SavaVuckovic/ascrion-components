import React from 'react';
import { useField } from 'formik';

import Input, { InputProps } from '../newtec-components/Input';

const Field: React.FC<InputProps> = ({ name, ...properties }) => {
  const [field, meta] = useField(name);
  const hasError = meta.error && meta.touched;

  return (
    <Input
      {...field}
      invalid={!!hasError}
      invalidMsg={meta.error}
      {...properties}
    />
  );
};

export default Field;
