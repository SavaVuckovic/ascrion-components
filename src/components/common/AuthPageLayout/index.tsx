import React from 'react';
import './style.scss';

const setImgStyle = (imageUrl: string): object => ({
  backgroundImage: `linear-gradient(180deg, rgba(32,33,40,0.3) 0%, rgba(32,33,40,0.6) 100%), url(${imageUrl})`,
});

const AuthPageLayout: React.FC<{ imageUrl: string }> = ({
  imageUrl,
  children,
}) => (
  <section className="container-fluid auth-section">
    <div className="row">
      <div className="brand col-lg-5" style={setImgStyle(imageUrl)}>
        <div className="brand-text">
          <h2 className="brand-title">
            Conference <b>2020</b>
          </h2>
          <h3 className="brand-location">Conpenhagen</h3>
        </div>
      </div>

      <div className="offset-lg-5 col-lg-7">
        <div className="row">{children}</div>
      </div>
    </div>
  </section>
);

export default AuthPageLayout;
