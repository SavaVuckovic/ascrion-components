import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './styles/index.scss';
import Register from './components/pages/Register';
import Login from './components/pages/Login';
// import EditProfile from './components/EditProfile';
import Testing from './components/Testing';

function App() {
  return (
    <Router>
      <Route path="/register" component={Register} />
      <Route path="/login" component={Login} />
      {/* <Route path="/profile/edit" component={EditProfile} /> */}
      <Route path="/testing" component={Testing} />
    </Router>
  );
}

export default App;
